$(document).ready(function(){
  // Set the global jquery ajax configs to synchronous (default is async)
  $.ajaxSetup({
    async: false
  });

  // Get data from donations.json
  $.getJSON("donations.json", function(donations) {
    this.indiegogo_total = 0;
    this.direct_total = 0;
    parent = this;

    $.each(donations, function(index, donation) {
      $.each(donation.indiegogo.usd, function(j,igg) {
        parent.indiegogo_total += igg.amount;
      });
      $.each(donation.direct.inr, function(j,dir) {
        parent.direct_total += dir.amount;
      });
    });

    // Direct donations are in INR, convert it to USD
    this.direct_total=(this.direct_total/70).toFixed(2);

    // Calculate percentage of fund raised out of total $1500 goal
    direct_percent = Math.floor((this.direct_total/1500)*100);
    indiegogo_percent = Math.floor((this.indiegogo_total/1500)*100);
    total_percent = direct_percent + indiegogo_percent;

    $("#direct-donations").css("width", direct_percent+"%");
    $("#indiegogo-donations").css("width", indiegogo_percent+"%");
    $("#direct-donations .percent").html(direct_percent+"%");
    $("#indiegogo-donations .percent").html(indiegogo_percent+"%");
  });

  // Show total percentage of fund raised under progress bar
  $(".progress-text").html("<span class='bold'>"+total_percent+"%</span> raised of <span class='bold'>$1500</span> goal");

  // Wait for json data to load and then show tooltip
  setTimeout(function(){
    $(".progress-bar").attr({"data-toggle":"tooltip","data-position":"top","data-trigger":"manual"});
    $(".progress-bar").tooltip('show');
  },600);
});
